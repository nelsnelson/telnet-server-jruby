#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end
#

# bundle exec docs/examples/custom_server.rb

require_relative '../../lib/telnet_server'

# Adapter class
class Adapter
  def reflect(method_reference, ctx, *args)
    information = "#{method_reference}(#{([ctx.channel] + args).join(', ')})"
    log.info information
    ctx.writeAndFlush("#{information}\r\n")
  end

  def channel_registered(ctx)
    reflect(__method__, ctx)
  end

  def channel_active(ctx)
    reflect(__method__, ctx)
  end

  def channel_unregistered(ctx)
    reflect(__method__, ctx)
  end

  def channel_idle(ctx, event)
    reflect(__method__, ctx, event)
  end

  def channel_read(ctx, msg)
    reflect(__method__, ctx, msg)
  end

  def message_received(ctx, msg)
    reflect(__method__, ctx, msg)
  end

  def user_event_triggered(ctx, event)
    reflect(__method__, ctx, event)
  end

  def exception_caught(ctx, cause)
    reflect(__method__, ctx, cause)
  end
end
# class Adapter

telnet_server = Telnet::Server.new
telnet_server.replace_listeners(Adapter.new)
telnet_server.run
