#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end
#

# bundle exec docs/examples/programmatic_client.rb

require_relative '../../lib/telnet_client'

Telnet::Client.new do |telnet|
  $stdout.write telnet.gets
  $stdout.puts 'Hello world!'
  telnet.puts 'Hello world!'
  $stdout.write telnet.gets
  $stdout.puts 'exit'
  telnet.puts 'exit'
  $stdout.write telnet.gets
  puts
end
