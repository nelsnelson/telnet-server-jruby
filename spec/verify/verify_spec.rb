# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'java'

unless defined?(PROJECT)
  VERIFY_DIR_PATH = File.expand_path(__dir__) unless defined?(VERIFY_DIR_PATH)
  SPEC_DIR_PATH = File.expand_path(File.dirname(VERIFY_DIR_PATH)) unless defined?(SPEC_DIR_PATH)
  PROJECT_DIR_PATH = File.expand_path(File.dirname(SPEC_DIR_PATH)) unless defined?(PROJECT_DIR_PATH)
  PROJECT = File.basename(PROJECT_DIR_PATH)
end

CHARSET = java.nio.charset.StandardCharsets::UTF_8

# rubocop: disable Metrics/AbcSize
# rubocop: disable Metrics/MethodLength
def execute_command(cmd, env)
  puts "Executing command: #{env.join(' ')} #{cmd.join(' ')}"
  process = java.lang.Runtime.getRuntime().exec(cmd.to_java(:string), env.to_java(:string))
  stdout = java.io.BufferedReader.new(
    java.io.InputStreamReader.new(process.getInputStream(), CHARSET)
  )
  sleep 0.1 until stdout.ready()
  puts "Process #{process} is alive"
  Thread.new do
    loop do
      line = begin
        stdout.readLine()
      rescue StandardError
        nil
      end
      if line.nil? || line.empty?
        break unless process.isAlive()
      else
        puts "[#{process}] #{line.chomp}"
      end
    end
  end
  process
end
# rubocop: enable Metrics/AbcSize
# rubocop: enable Metrics/MethodLength

# rubocop: disable Metrics/BlockLength
RSpec.describe 'verify gem' do
  let(:gem_path) { 'tmp' }
  it 'should support an echo telnet server' do
    # cmd = %w[telnet --verbose]
    cmd = %w[./tmp/bin/telnet --verbose]
    env_path = ENV.fetch('PATH')
    paths = [File.join(gem_path, 'bin')]
    paths.unshift(env_path) unless env_path.nil?
    env = %W[GEM_PATH=#{gem_path} PATH=#{paths.join(':')}]
    process = execute_command(cmd, env)
    timeout = 10
    start = Time.now
    results = ''
    begin
      require_relative '../../lib/telnet_client'
      Telnet::Client.new do |telnet|
        results << telnet.gets
        telnet.puts 'Hello world!'
        results << telnet.gets
        telnet.puts 'exit'
      end
    rescue Interrupt => e
      warn "\n#{e.class}"
      exit
    rescue Errno::ECONNREFUSED => e
      warn e.message
      sleep 1
      retry unless (Time.now - start) > timeout
    rescue StandardError => e
      warn "Unexpected error: #{e.class} #{e.message}"
      e.backtrace.each { |t| warn t }
    end
    expected_results = Regexp.new([
      'Welcome to .+!',
      'The time is .+\.',
      '>Server echo: Hello world!',
      '>'
    ].join('\r\n'))
    expect(results).to match(expected_results)
  rescue Interrupt => e
    puts "\n#{e.class}"
  rescue StandardError => e
    warn "Unexpected error: #{e.class} #{e.message}"
    raise e
  ensure
    if defined?(process) && !process.nil?
      begin
        puts "Destroying process #{process}"
        process.destroy()
        process.destroyForcibly() if process.isAlive()
      rescue StandardError => e
        warn "Unexpected error: #{e.class} #{e.message}"
        raise e
      end
    end
  end
  # it 'should support an echo telnet server'

  it 'should support a simple programmatic telnet server' do
    require 'telnet-server'
    server = Telnet::Server.new
    Thread.new do
      server.run
    end
    sleep 2
    server.stop
    sleep 2
  end
  # it 'should support a simple programmatic telnet server'
end
# RSpec.describe 'verify gem'
# rubocop: enable Metrics/BlockLength
