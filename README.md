# telnet-server-jruby

[![License](https://img.shields.io/badge/license-MIT--2.0-blue.svg?style=flat)][license]

This is a small telnet server for [JRuby].

It is based on the [Netty project].  Netty is written in java, but I wanted to write ruby.


## Quick-start

Follow these instructions to get a telnet server echo program running.


### Container

You may run the telnet server in a container.

```sh
colima start
docker-compose up --detach
nc localhost 21
docker-compose down
```


Building the image or running the container:

```sh
docker build --tag telnet-server-jruby .
docker run --detach --publish 21:21 --name telnet-server-jruby telnet-server-jruby
```


## Manually

Run directly with the required dependencies installed.


## Install mise-en-place

The [mise] CLI tool used to manage multiple runtime versions.

See: https://mise.jdx.dev/getting-started.html

```sh
curl https://mise.jdx.dev/install.sh | sh
~/.local/bin/mise --version
mise 2024.x.x
```

Enable mise activation in future zsh sessions.

```sh
echo 'eval "$(~/.local/bin/mise activate zsh)"' >> ~/.zshrc
```


## Install required runtime software

Use mise to install the runtime software defined as requirements
in the .tool-versions file.

```sh
mise install
```


Install the project dependencies.

```sh
gem install bundler
bundle install
```


## Run

The entrypoint for the web application service may now be invoked from a command line interface terminal shell.

```sh
bundle exec ./telnet.rb &
nc localhost 21
```


## Build the gem

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```sh
bundle exec rake
```


## Publish the gem

To publish the gem, execute:

```sh
bundle exec rake publish
```


## Project file tree

Here is a bird's-eye view of the project layout.

```sh
# date && tree -A -I "logs|vendor|tmp|Gemfile.lock"
Sun Jul 24 14:18:45 CDT 2022
.
├── Dockerfile
├── Gemfile
├── LICENSE
├── README.md
├── Rakefile
├── docker-compose.yaml
├── docs
│   └── examples
│       ├── custom_server.rb
│       └── programmatic_client.rb
├── exe
│   └── telnet
├── lib
│   ├── log.rb
│   ├── telnet
│   │   ├── argument_parser.rb
│   │   ├── config.rb
│   │   ├── instance_methods.rb
│   │   ├── server.rb
│   │   └── version.rb
│   ├── telnet-server.rb
│   ├── telnet_client.rb
│   └── telnet_server.rb
├── spec
│   ├── spec_helper.rb
│   ├── test_spec.rb
│   └── verify
│       └── verify_spec.rb
├── telnet-server-1.0.4-java.gem
├── telnet-server-jruby.gemspec
├── telnet-server-jruby.png
└── telnet.rb

7 directories, 25 files
```


## CI linting

Use the GitLab CI Linting API to validate the syntax of a CI definition file.

```sh
jq --null-input --arg yaml "$(<.gitlab/ci/gem.gitlab-ci.yml)" '.content=$yaml' | curl --silent --location https://gitlab.com/api/v4/ci/lint --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --header "Content-Type: application/json" --data @- | jq --raw-output '.errors[0]'
```


## CI configuration

Generate a deploy key.

```sh
ssh-keygen -t ed25519 -P '' -C deploy_key -f deploy_key_ed25519
```

Use the GitLab Project-level Variables API to add the deploy key as a ssh private key variable.

```sh
project_path="nelsnelson/$(basename $(pwd))"

# Test auth token validity
curl --silent --show-error --location "https://gitlab.com/api/v4/projects" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq '.[0]["id"]'

project=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_path}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --arg project_path "${project_path}" '.[] | select(.path_with_namespace == $project_path)')

project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_path}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --arg project_path "${project_path}" '.[] | select(.path_with_namespace == $project_path) | .id')

# Add the deploy_token as a CI variable:
curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/variables" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=SSH_PRIVATE_KEY" --form "value=$(cat ./deploy_key_ed25519)" --form "protected=true" | jq
```

Use the Deploy keys API to add a the public deploy key as a deploy key for the project.

```sh
curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/deploy_keys" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "title=deploy_key"  --form "key=$(cat ./deploy_key_ed25519.pub)" --form "can_push=true" | jq
```

[license]: https://gitlab.com/nelsnelson/telnet-server-jruby/blob/master/LICENSE
[mise]: https://mise.jdx.dev/
[Netty project]: https://github.com/netty/netty
[Java JDK]: https://www.java.com/en/download/
[JRuby]: https://jruby.org/download
