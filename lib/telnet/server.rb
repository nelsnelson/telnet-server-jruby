# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'
require 'tcp-server'

require_relative 'instance_methods'

# The Telnet module
module Telnet
  # The Server class sets up the netty server
  class Server < Server::Server
    include ::Telnet::InstanceMethods

    def initialize(options = {}, *handlers, &block)
      raise ArgumentError, 'Parameter may not be nil: options' if options.nil?
      super(::Telnet.server_config.merge(options), *handlers, &block)
    end
  end
end
# module Telnet
