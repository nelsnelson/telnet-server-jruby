# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The Telnet module
module Telnet
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import java.net.InetAddress

  # The InstanceMethods module
  module InstanceMethods
    def channel_active(ctx)
      host_name = InetAddress.local_host.host_name
      # Send greeting for a new connection.
      ctx.write "Welcome to #{host_name}!\r\n"
      ctx.write "The time is #{Time.now}.\r\n"
      ctx.write @options[:prompt]
      ctx.flush
    end

    def message_received(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      msg&.chomp!
      log.info "Received message: #{msg}"
      return super(ctx, msg) unless respond_to?(:handle_message)
      handle_message(ctx, msg)
    end

    # Generate and write a response.
    # Writing to a ChannelBuffer is not needed here.
    # The encoder will do the conversion.
    def handle_message(ctx, message)
      request = message.to_s.strip
      return handle_empty_request(ctx) if request.empty?
      return close(ctx) if quit_command?(request)
      ctx.write "Server echo: #{request}\r\n"
      ctx.write @options[:prompt]
    end

    def handle_empty_request(ctx)
      ctx.write "Please type something.\r\n"
      ctx.write @options[:prompt]
    end

    def quit_command?(request)
      @options[:quit_commands].include?(request.to_s.downcase.to_sym)
    end

    def close(ctx)
      future = ctx.write "Bye!\r\n"
      # Close the connection
      future.addListener(ChannelFutureListener::CLOSE)
    end

    def channel_read_complete(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      ctx.flush
    end

    def exception_caught(ctx, cause)
      cause.printStackTrace()
      ctx.close
    end
  end
  # module HandlerMethods
end
# module Telnet
