# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The Telnet module
module Telnet
  DEFAULT_PORT = 21
  DEFAULT_PROMPT = '>'.freeze

  def server_config
    @server_config ||= ::Server.server_config.merge(
      {
        port: DEFAULT_PORT,
        prompt: DEFAULT_PROMPT
      }
    )
  end
  module_function :server_config
end
