#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'tcp-server'

require_relative 'logging'
require_relative 'telnet/argument_parser'
require_relative 'telnet/server'

# The Telnet module
module Telnet
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    ::Telnet::Server.new(args).run
  rescue Interrupt => e
    warn format("\r%<class>s", class: e.class)
    exit
  rescue StandardError => e
    ::Telnet::Server.log.fatal(e.message)
    abort
  end
end
# module Telnet

Object.new.extend(Telnet).main if $PROGRAM_NAME == __FILE__
