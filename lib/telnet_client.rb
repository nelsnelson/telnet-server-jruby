#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'optparse'

require 'java'
require 'netty'

require_relative 'logging'
require_relative 'telnet/version'

# Simple telnet client

# The TelnetClient module
module TelnetClient
  VERSION = '1.0.2'.freeze unless defined?(VERSION)
end

# The Telnet module
module Telnet
  def client_config
    @client_config ||= {
      host: '0.0.0.0',
      port: 21,
      ssl: false,
      log_level: Logger::INFO,
      quit_commands: %i[bye cease desist exit leave quit stop terminate],
      max_frame_length: 8192,
      delimiter: Java::io.netty.handler.codec.Delimiters.nulDelimiter
    }
  end
  module_function :client_config
end
# module Telnet

# The Telnet module
module Telnet
  CHANNEL_TYPE = Java::io.netty.channel.socket.nio.NioSocketChannel.java_class
  java_import Java::io.netty.bootstrap.Bootstrap
  java_import Java::io.netty.channel.nio.NioEventLoopGroup
  java_import Java::io.netty.handler.ssl.SslContext
  java_import Java::io.netty.handler.ssl.SslContextBuilder
  java_import Java::io.netty.handler.ssl.util.InsecureTrustManagerFactory

  # The ClientInitializationMethods module
  module ClientInitializationMethods
    def init(options)
      @options = options
      @host = options[:host]
      @port = options[:port]
      @queue = java.util.concurrent.LinkedBlockingQueue.new
    end

    def bootstrap
      @bootstrap = Bootstrap.new
      @bootstrap.group(client_group)
      @bootstrap.channel(::Telnet::CHANNEL_TYPE)
      @bootstrap.handler(logging_handler) if @options[:log_requests]
      @bootstrap.handler(channel_initializer)
    end

    def client_group
      @client_group ||= NioEventLoopGroup.new
    end

    def logging_handler
      @logging_handler ||= LoggingHandler.new(LogLevel::INFO)
    end

    def channel_initializer
      @channel_initializer ||= ::Telnet::ClientChannelInitializer.new(@host, @port, @options)
    end

    def configure_handlers(*handlers, &block)
      channel_initializer.default_handler.add_listener(self)
      channel_initializer.default_handler.listeners.addAll(handlers)
      @user_app = block
      @application_handler = lambda do |ctx, msg|
        if @user_app.nil? || @user_app.arity == 1
          @queue.add(msg)
        else
          @user_app.call(ctx, msg)
        end
      end
    end
  end
  # module ClientInitializationMethods
end
# module Telnet

# The Telnet module
module Telnet
  java_import Java::io.netty.buffer.Unpooled
  java_import Java::io.netty.channel.AbstractChannel

  # The ClientInstanceMethods module
  module ClientInstanceMethods
    def puts(msg)
      wait_until_channel_is_active
      msg.chomp!
      log.trace "#puts msg: #{msg.inspect}"
      return if msg.nil? || msg.empty?
      @last_write_future = @channel.writeAndFlush("#{msg}\r\n")
    end

    def gets(timeout = nil)
      log.debug 'Waiting for response from server'
      timeout.nil? ? @queue.take : @queue.take(timeout)
    rescue StandardError => e
      warn "Unexpected error waiting for message: #{e.message}"
      nil
    end

    def wait_until_channel_is_active(timeout = 5, give_up = Time.now + timeout)
      sleep 0.1 until @channel.active? || Time.now > give_up
    end

    def connect
      @channel = bootstrap.connect(@host, @port).sync().channel()
      @last_write_future&.sync()
    rescue AbstractChannel::AnnotatedConnectException => e
      raise e.message
    rescue StandardError => e
      raise "Connection failure: #{e.message}"
    end

    def close(channel = @channel)
      log.debug 'Closing primary channel'
      channel.closeFuture().sync()
    ensure
      shutdown
    end

    def shutdown
      log.debug 'Shutting down gracefully'
      @client_group&.shutdownGracefully()
    ensure
      client_has_shut_down
    end

    def session
      when_client_has_shut_down(@client_group) do |group|
        log.debug "Channel group has shut down: #{group.inspect}"
      end
      @user_app.nil? ? read_user_commands : invoke_user_app
    end

    def invoke_user_app
      @user_app&.call(self)
    ensure
      close
    end

    def shut_down_callbacks
      @shut_down_callbacks ||= []
    end

    def when_client_has_shut_down(*args, &block)
      shut_down_callbacks << {
        block: block,
        args: args
      }
      self
    end

    def client_has_shut_down
      shut_down_callbacks.take_while do |callback|
        callback[:block]&.call(*callback.fetch(:args, []))
      end
    rescue StandardError => e
      log.error e.message
    end

    def channel_unregistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      shutdown
    end

    def channel_active(ctx)
      log.info "Connected to #{ctx.channel().remoteAddress0()}"
    end

    def channel_inactive(ctx)
      log.info "Disconnected from #{ctx.channel().remoteAddress0()}"
    end

    def message_received(ctx, message)
      log.debug "##{__method__} message: #{message}"
      notify :message_received, ctx, message
      if @application_handler.nil?
        $stdout.print message.chomp unless message.nil?
      else
        @application_handler.call(ctx, message)
      end
    end

    def execute_command(str, client = self)
      return if str.empty?
      client.puts "#{str}\r\n"
      close if @options[:quit_commands].include?(str.downcase.to_sym)
    end

    def read_user_commands
      log.trace 'Reading user commands'
      loop do
        log.debug 'Waiting for user input...'
        input = $stdin.gets&.chomp
        raise 'Poll failure from stdin' if input.nil?
        break unless @channel.active?
        break if execute_command(input).is_a?(AbstractChannel::CloseFuture)
      end
    end
  end
  # module ClientInstanceMethods
end
# module Telnet

# The Telnet module
module Telnet
  # The Listenable module
  module Listenable
    def listeners
      @listeners ||= java.util.concurrent.CopyOnWriteArrayList.new
    end

    def add_listener(listener)
      listeners << listener
    end

    def remove_listener(listener)
      listeners.delete(listener)
    end

    def notify(message, *args)
      return if listeners.empty?
      log.trace "Notifying listeners (#{listeners}) of message: #{message}"
      listeners.each do |listener|
        listener.send(message.to_sym, *args) if listener.respond_to?(message.to_sym)
      end
    end
  end
end

# The Telnet module
module Telnet
  java_import Java::io.netty.channel.SimpleChannelInboundHandler

  # The ModularHandler class
  class ModularHandler < SimpleChannelInboundHandler
    include ::Telnet::Listenable

    def isSharable
      true
    end

    def channelRegistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_registered, ctx
      super(ctx)
    end

    def channelUnregistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_unregistered, ctx
      super(ctx)
    end

    def channelActive(ctx)
      ::Telnet.log.info "Channel active #{ctx.channel}"
      notify :channel_active, ctx
      super(ctx)
    end

    def channelInactive(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_inactive, ctx
      super(ctx)
    end

    def messageReceived(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :message_received, ctx, msg
    end

    def channelRead(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :channel_read, ctx, msg
      super(ctx, msg)
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected abstract void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception'
    def channelRead0(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      messageReceived(ctx, msg)
    end

    def channelReadComplete(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_read_complete, ctx
      super(ctx)
    end

    def channelWritabilityChanged(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_writability_changed, ctx
      super(ctx)
    end

    def userEventTriggered(ctx, evt)
      log.trace "##{__method__} channel: #{ctx.channel}, event: #{evt}"
      notify :user_event_triggered, ctx, evt
      super(ctx, evt)
    end

    def exceptionCaught(ctx, cause)
      ::Telnet.log.warn "##{__method__} channel: #{ctx.channel}, cause: #{cause.message}"
      listeners = notify :exception_caught, ctx, cause
      super(ctx, cause) if listeners.empty?
    end

    IdentiferTemplate = '#<%<class>s:0x%<id>s>'.freeze

    def to_s
      format(IdentiferTemplate, class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
  # class ModularHandler
end
# module Telnet

# The Telnet module
module Telnet
  java_import Java::io.netty.handler.codec.DelimiterBasedFrameDecoder
  java_import Java::io.netty.handler.codec.string.StringDecoder
  java_import Java::io.netty.handler.codec.string.StringEncoder

  # The ClientChannelInitializer class
  class ClientChannelInitializer < Java::io.netty.channel.ChannelInitializer
    attr_accessor :decoder, :encoder

    def initialize(host, port, options = {})
      super()
      @host = host
      @port = port
      @options = options
      @decoder = StringDecoder.new
      @encoder = StringEncoder.new
    end

    def default_handler
      @default_handler ||= ::Telnet::ModularHandler.new
    end

    def initChannel(channel)
      pipeline = channel.pipeline()
      pipeline.addLast(ssl_handler(channel)) if @options[:ssl]
      # pipeline.addLast(frame_decoder)
      pipeline.addLast(decoder)
      pipeline.addLast(encoder)
      pipeline.addLast(default_handler)
    end

    def frame_decoder
      DelimiterBasedFrameDecoder.new(@options[:max_frame_length], @options[:delimiter])
    end

    def ssl_handler(channel)
      ssl_context&.newHandler(channel.alloc(), @host, @port)
    end

    def ssl_context
      nil # TODO: Implement
    end
  end
  # class ClientChannelInitializer
end
# module Telnet

# The Telnet module
module Telnet
  # The Telnet::Client class
  class Client
    include ::Telnet::ClientInitializationMethods
    include ::Telnet::ClientInstanceMethods
    include ::Telnet::Listenable

    def initialize(params = {}, &block)
      init(::Telnet.client_config.merge(params.fetch(:options, {})))
      configure_handlers(params.fetch(:handlers, []), &block)
      connect
      session
    end

    IdentiferTemplate = '#<%<class>s:0x%<id>s>'.freeze

    def to_s
      format(IdentiferTemplate, class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
end
# module Telnet

# The Telnet module
module Telnet
  # The ArgumentsParser class
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(parser = OptionParser.new, options = ::Telnet.client_config.dup)
      @parser = parser
      @options = options
      @flags = %i[banner ssl log_level help version]
      @flags.each { |method_name| method(method_name)&.call if respond_to?(method_name) }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options] <hostname> [port]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def log_level
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] ||= 0
        @options[:log_level] -= 1
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{::TelnetClient::VERSION}"
        exit
      end
    end

    def validated_port(val)
      raise OptionParser::InvalidArgument, "Invalid port: #{val}" unless \
        /^\d+$/.match?(val.to_s) && val.positive? && val < 65_536

      val
    end

    def parse_positional_arguments!
      @options[:host] = ARGV.shift or raise OptionParser::MissingArgument, 'hostname'
      return if (given_port = ARGV.shift&.to_i).nil?

      @options[:port] = @parser.validated_port(given_port).to_i
    end
  end
  # class ArgumentsParser

  def parse_arguments(arguments_parser = ArgumentsParser.new)
    arguments_parser.parser.parse!(ARGV)
    arguments_parser.parse_positional_arguments!
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module Telnet

# The Telnet module
module Telnet
  # The ConsoleHandler class
  class ConsoleHandler
    def message_received(ctx, message)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{message}"
      log.debug "Received message: #{message}"
      $stdout.print message unless message.nil?
    end
  end
end
# module Client

# The Telnet module
module Telnet
  # rubocop: disable Metrics/AbcSize
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    ::Telnet::Client.new(options: args, handlers: ::Telnet::ConsoleHandler.new)
  rescue Interrupt => e
    warn "\n#{e.class}"
    exit
  rescue StandardError => e
    ::Telnet::Client.log.fatal "Unexpected error: #{e.class}: #{e.message}"
    e.backtrace.each { |t| log.debug t } if Logging.log_level == Logger::DEBUG
    exit 1
  end
  # rubocop: enable Metrics/AbcSize
end
# module Telnet

Object.new.extend(Telnet).main if $PROGRAM_NAME == __FILE__
