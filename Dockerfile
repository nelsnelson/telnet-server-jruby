FROM jruby:9.4.6.0

WORKDIR /usr/src/telnet-server-jruby

COPY . .
RUN bundle install \
  && \
  rm Gemfile.lock

CMD ["./telnet.rb"]
